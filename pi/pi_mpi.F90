program pi_mpi
  use types_mod 
  use mpi

#if defined(_OPENMP)
  use omp_lib
#endif

  implicit none
  integer(kind = DP), parameter :: N = 100000000000
  integer(kind = DP) :: i
  integer(kind = SP) :: rank, size, ierr, num_omp_threads
  real(kind = DP) :: dx, pi_estimate, pi_exact, pi_partial, x, pi_diff
  real(kind = DP) :: t1, t2, dt

  call MPI_Init( ierr )
  call MPI_Comm_rank( MPI_COMM_WORLD, rank, ierr )
  call MPI_Comm_size( MPI_COMM_WORLD, size, ierr )

  dx = 1.0_DP / dble( N )
  pi_partial = 0.0_DP
  pi_exact = 3.141592653589793238462643_DP
  num_omp_threads = 1

#if defined(_OPENMP)
!$omp parallel shared(num_omp_threads)
  num_omp_threads = omp_get_num_threads( )
!$omp end parallel
  print *, 'OpenMP enabled. num threads = ', num_omp_threads
#endif

  t1 = MPI_Wtime( )

!$omp parallel do private(i, x) reduction(+ : pi_partial)
  do i = rank, N-1, size
    x = (dble( i ) + 0.5_DP) * dx
    pi_partial = pi_partial + f( x )
  end do

  pi_partial = pi_partial * dx

  call MPI_Reduce( pi_partial, pi_estimate, 1, MPI_DOUBLE, MPI_SUM, 0, &
                   MPI_COMM_WORLD, ierr )

  pi_diff = dabs( pi_exact - pi_estimate )

  t2 = MPI_Wtime( )
  dt = t2 - t1

  if ( rank == 0 ) then
    print *, 'size', size * num_omp_threads
    print *, 'pi_exact', pi_exact
    print *, 'pi_estimate', pi_estimate
    print *, 'pi_diff', pi_diff
    print *, 'time (seconds)', dt
  end if

  call MPI_Finalize( ierr )

  contains

  function f( x )
    use types_mod

    implicit none
    real(kind = DP) :: f
    real(kind = DP), intent(in) :: x

    f = 4.0_DP / ( 1.0_DP + x * x )
  end function f

end program pi_mpi
  
