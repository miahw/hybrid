#!/usr/bin/python

import os
import sys
import optparse
import numpy as np
import matplotlib as mpl

mpl.use( 'Agg' )
import matplotlib.pyplot as plt

from matplotlib import rcParams
rcParams.update( { 'figure.autolayout' : True } )

parser = optparse.OptionParser( )

parser.add_option( '--title', dest = 'title', default = 'TITLE' )
parser.add_option( '--start-size', dest = 'start_size', default = 0 )
parser.add_option( '--input-file', dest = 'input_file', default = '' )
parser.add_option( '--output-image', dest = 'output_image', default = 'scalability.png' )

options, remainder = parser.parse_args( )

if not os.path.exists( options.input_file ):
   raise Exception( 'Input file "' + options.input_file + '" does not exist' )

data = np.genfromtxt( options.input_file, dtype = [ ( 'cores', 'i8' ),         \
                                                    ( 'mpi_1_omp_0', 'f8' ),   \
                                                    ( 'mpi_1_omp_1', 'f8' ),   \
                                                    ( 'mpi_1_omp_16', 'f8' ),  \
                                                    ( 'mpi_2_omp_8', 'f8' ) ], \
                                         comments = '#' )

cores        = data['cores']
mpi_1_omp_0  = data['mpi_1_omp_0']
mpi_1_omp_1  = data['mpi_1_omp_1']
mpi_1_omp_16 = data['mpi_1_omp_16']
mpi_2_omp_8  = data['mpi_2_omp_8']

start = int( options.start_size )
labels = [ str( x ) for x in cores[start:] ]

fig, plt1 = plt.subplots( )

l1, = plt1.plot( cores, mpi_1_omp_0[start:] )
l2, = plt1.plot( cores, mpi_1_omp_1[start:] )
l3, = plt1.plot( cores, mpi_1_omp_16[start:] )
l4, = plt1.plot( cores, mpi_2_omp_8[start:] )

plt1.set_ylabel( 'MFLOP/s', fontsize = 10 )
# plt1.set_yscale( 'log' )
plt1.grid( True )
plt1.set_xlabel( 'CPU cores', fontsize = 10 )

for t1 in plt1.get_yticklabels( ):
   t1.set_color( 'black' )
   t1.set_fontsize( 10 )

for xt in plt1.get_xticklabels( ):
  xt.set_color( 'black' )
  xt.set_fontsize( 10 )
# rotate the xtics by 45 degrees
  xt.set_rotation( 0 )


plt.legend( ( l1, l2, l3, l4 ), ( 'MPI=16 + OMP=0', 'MPI=1 + OMP=1', 'MPI=1 + OMP=16', 'MPI=2 + OMP=8' ), \
                                 loc = 'upper left', fontsize = 10 )
plt.xscale( 'log', basex = 2 )
# plt.axis( xmin = start, xmax = 22 )
# plt.xticks( cores, labels )
plt.title( options.title, fontsize = 10 )
plt.savefig( options.output_image )
